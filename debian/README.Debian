rkhunter
--------

* RKHUNTER FAQ
  An updated Rootkit Hunter FAQ can be found online at:
  http://rkhunter.cvs.sourceforge.net/viewvc/*checkout*/rkhunter/rkhunter/files/FAQ

* LOCAL CONFIGURATION FILE
  It is possible to put configuration changes into a local config
  file. This file, called 'rkhunter.conf.local', must be in the same
  directory as the main configuration file (ie. in /etc/ by default for
  the Debian package). Rkhunter will look for configuration options in the
  main config file, and then in the local config file if it exists. For options
  allowed only once, the last one seen is used. For options allowed more than
  once, all options from both files will be used.

  Please note that this file is not part of the Debian package, and is hence not
  dealt with when the package is upgraded or purged.

* FALSE POSITIVES
  Below is a list of packages which are known to set off false alarms in
  rkhunter:

    * slice: /usr/bin/slice sets false alarm about RH-Sharpe
    * sash: as the root account is cloned to sashroot, rkhunter issues a
      warning telling the sashroot account has UID=0.
      If you have deliberately installed sash, you can avoid this warning thanks
      to the UID0_ACCOUNTS configuration option in /etc/rkhunter.conf.
    * hdparm: the string "hdparm" found in the initscripts leads to rkhunter warns
      about possible Xzibit rootkit. Use the RTKT_FILE_WHITELIST option to whitelist
      initscripts stating this string (eg. /etc/init.d/hdparm)
    * IRC daemons trigger warnings about possible rogue IRC bot. You wan whitelist
      the TCP port 6667, or, better, tell rkhunter to trust the executable, using the
      PORT_WHITELIST option, eg: PORT_WHITELIST="/usr/bin/znc"

  --

  Below is a list of common hidden files and directories known to set off
  false alarms in rkhunter:

    * /dev/.static/, /dev/.udev & /dev/.udevdb/ - used by udev
    * /etc/.java/ - it is common for java installations to use this
      hidden directory
    * /dev/.initramfs - created by initramfs-tools generated ramfs
      filesystems during boot

  In most cases, you can just ignore warnings about these files and directories.
  Use ALLOWHIDDENFILE and ALLOWHIDDENDIR options in /etc/rkhunter.conf to
  avoid them.


* HASH CHECKS
  By default, all hashes checks are now ENABLED in the standard daily cron
  job.

  Add the 'hashes' and 'attributes' tests to the DISABLE_TESTS option in
  /etc/rkhunter.conf if you wish to disable them.

  If enabled, each time a base package is upgraded, you will have to run:
  'rkhunter --propupd' to update the file properties database located
  in /var/lib/rkhunter/db/rkhunter.dat.

  This can be done automatically after each install/remove. Please run:
    # dpkg-reconfigure rkhunter
  to enable this feature.

  ** SECURITY WARNING:
     When using automatic database update after each package install/upgrade,
     an attacker could replace a file after it is installed, and before --propupd
     is run. On a highly protected machine, it is recommended to disable the 
     automatic database update.

  Note that if both 'hashes' and 'attributes' tests are disabled, this feature
  will be automatically disabled. This is also the case if the group test
  'properties' is disabled.

  ** SECURITY WARNING:
     It is the users' responsibility to ensure that the files on the system are genuine and from
     a reliable source. rkhunter can only report if a file has changed, but not on what has
     caused the change. Hence, if a file has changed, and  the  --propupd  command
     option is used, then rkhunter will assume that the file is genuine.


* WEEKLY DATABASE UPDATES
  To be able to run automatic database update, you will need a tool with which to
  download file updates.

  Currently wget, curl, (e)links, lynx and GET are supported.

  You will also need to reconfigure rkhunter to activate the automatic weekly database update:
    # dpkg-reconfigure rkhunter


* THIRD PARTY TOOLS

  rkhunter also supports two 3rd party tools which are not in debian:

    * skdet - http://www.xs4all.nl/~dvgevers/ (*)
    Skdet is a simple program that will detect the following rootkits:
    - SucKIT (<=1.3b)
    - adore (all versions)
    - adore-ng (all versions)
    - UNFshit (<=1.1a)
    - UNFkmem (from phrack.org)
    - frontkey (first release)
    - all rootkits that use trojaned files

    Note that Skdet seems to be unmaintained, and is probably obsolete.
    USE AT YOUR OWN RISK

    The license of skdet is unknown which prevents it from being packaged for Debian.

    (*) The original skdet website is down, this is an archived copy


-- Micah Anderson <micah@debian.org>  Sat, Sep 3 18:23:21 CDT 2005
-- Julien Valroff <julien@debian.org>  Sun, 20 Mar 2011 08:06:29 +0100
