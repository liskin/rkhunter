#! /bin/sh
# postinst script for rkhunter

set -e

. /usr/share/debconf/confmodule

case "$1" in
    configure)

        tempfile=`mktemp`
        cp -p /usr/share/rkhunter/default.conf ${tempfile}

        # Merge debconf values into the configuration
        for foo in CRON_DB_UPDATE CRON_DAILY_RUN APT_AUTOGEN; do
                template=$(echo ${foo} | tr '[:upper:]' '[:lower:]')
                db_get rkhunter/${template}
                sed -i -re "s@^(${foo}=).*@\1\"${RET}\"@" "$tempfile"
        done

        ucf --debconf-ok ${tempfile} /etc/default/rkhunter
        ucfr rkhunter /etc/default/rkhunter
        rm -f ${tempfile}

        # Copy the passwd/group files to the TMP directory
        # to avoid warnings when rkhunter is first run.
        # This is normally done by the installer script.
        rkhtmpdir=/var/lib/rkhunter/tmp
        if [ -e "/etc/rkhunter.conf" ]; then
            rkhtmpdir=$(grep '^TMPDIR' /etc/rkhunter.conf | sed 's/TMPDIR=//')
        fi
        [ -f $rkhtmpdir/passwd ] || cp -p /etc/passwd $rkhtmpdir >/dev/null 2>&1
        [ -f $rkhtmpdir/group ] || cp -p /etc/group $rkhtmpdir >/dev/null 2>&1

        # Only update the file properties database if the hashes and attributes
        # tests are not disabled either in /etc/rkhunter.conf AND if the automatic
        # database update is disabled in case of an upgrade (in order to avoid calling
        # --propupd twice (see #471389)
        #
        # TODO: check what needs to be done on reconfigure
        # (note: debconf sets $DEBCONF_RECONFIGURE=1 when a package is reconfigured)

        # UPGRADE
        if [ -n "$2" ]; then
            if [ "$APT_AUTOGEN" = "false" ]; then
                /usr/share/rkhunter/scripts/rkhupd.sh || true
            fi

        # CLEAN INSTALL
        else
            /usr/share/rkhunter/scripts/rkhupd.sh || true
        fi
    ;;

    triggered)
        # only active if APT_AUTOGEN is disabled
        grep -qiE '^APT_AUTOGEN=.?(true|yes)' /etc/default/rkhunter || /usr/share/rkhunter/scripts/rkhupd.sh || true
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)

    ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

#DEBHELPER#

exit 0
