# rkhunter po-debconf translation to Spanish
# Copyright (C) 2006, 2008 Software in the Public Interest
# This file is distributed under the same license as the rkhunter package.
#
# Changes:
#   - Initial translation
#       Javier Ruano Ruano , 2006
#
#   - Updates
#       Francisco Javier Cuadrado, 2008
#
#  Traductores, si no conoce el formato PO, merece la pena leer la
#  documentación de gettext, especialmente las secciones dedicadas a este
#  formato, por ejemplo ejecutando:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
#   http://www.debian.org/intl/spanish/
#   especialmente las notas y normas de traducción en
#   http://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
#   /usr/share/doc/po-debconf/README-trans
#   o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Si tiene dudas o consultas sobre esta traducción consulte con el último
# traductor (campo Last-Translator) y ponga en copia a la lista de
# traducción de Debian al epañol (<debian-l10n-spanish@lists.debian.org>)
#
msgid ""
msgstr ""
"Project-Id-Version: rkhunter 1.3.2-6\n"
"Report-Msgid-Bugs-To: Source: rkhunter@packages.debian.org\n"
"POT-Creation-Date: 2007-12-12 01:13+0530\n"
"PO-Revision-Date: 2008-11-11 09:15+0100\n"
"Last-Translator: Francisco Javier Cuadrado <fcocuadrado@gmail.com>\n"
"Language-Team: Debian L10n Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Activate daily run of rkhunter?"
msgstr "¿Desea activar la ejecución diaria de rkhunter?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"If you choose this option, rkhunter will be run automatically by a daily "
"cron job."
msgstr ""
"Si elige esta opción, rkhunter se ejecutará automáticamente mediante una "
"tarea diaria de cron."

#. Type: boolean
#. Description
#: ../templates:3001
msgid "Activate weekly update of rkhunter's databases?"
msgstr ""
"¿Desea activar la actualización semanal de la base de datos de rkhunter?"

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"If you choose this option, rkhunter databases will be updated automatically "
"by a weekly cron job."
msgstr ""
"Si selecciona esta opción, las bases de datos de rkhunter se actualizarán de "
"forma automática por medio de una tarea semanal de cron."

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Automatically update rkhunter's file properties database?"
msgstr ""
"¿Actualizar la base de datos de las propiedades de archivos de rkhunter "
"automáticamente?"

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"The file properties database can be updated automatically by the package "
"management system."
msgstr ""
"La base de datos de las propiedades de archivos se puede actualizar "
"automáticamente mediante el sistema de gestión del paquete."

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"This feature is not enabled by default as database updates may be slow on "
"low-end machines. Even if it is enabled, the database won't be updated if "
"the 'hashes' test is disabled in rkhunter configuration."
msgstr ""
"Esta característica no está activada de manera predeterminada, ya que las "
"actualizaciones de la base de datos pueden ser lentas en máquinas antiguas. "
"Incluso si está activada, la base de datos no se actualizará si el test de "
"«hashes» está desactivado en la configuración de rkhunter."
